<?php
/**
 * Colección de metodos de una baraja
  **/
require_once 'Carta.php';

class Baraja {

    /******************************************************/
    /********************** ATRIBUTOS *********************/
    /******************************************************/
    private $cartas = array();

    /******************************************************/
    /*********************** METODOS **********************/
    /******************************************************/


    function __construct() //aqui se consume el metodo agregarCarta para popular el array cartas y asi crear la baraja
    {
        $array_cartas = array("As","Dos","Tres","Cuatro","Cinco","Seis","Siete","Sota","Caballo","Rey");
        $array_valores = array(1,2,3,4,5,6,7,0.5);
        $array_palos = array("Bastos","Copas","Espadas","Oros");
        $count_cartas = count($array_cartas); 
        
        foreach ($array_palos as $key => $value)
        {
            for ($i=0; $i <$count_cartas ; $i++)
            { 
                $valor = ($i <= 6)? $array_valores[$i] : $array_valores['7'] ;
                $this->agregarCarta($array_cartas[$i], floatval($valor),$value);
            } 
        }
    }

    /**
     * [agrega consume el objeto carta y agrega un registro a el array cartas]
     * @return []         
     */
    public function agregarCarta($nombre, $valor, $palo) {
        $carta = new Carta ($nombre, $valor, $palo);
        $this->cartas[] = $carta;
    }

    /**
     * [saca un carta de la baraja y reordena el array de cartas]
     * @return []         
     */
    public function sacarCarta($carta) {
        unset($this->cartas[$carta]);
        $this->cartas = array_values($this->cartas);
    }
    

    /**
     * [toma una carta de la baraja y la elimina para que no vuelve a ser usuada]
     * @return [objeto carta seleccionada]         
     */
    public function getCarta() {
        $indice = rand(0, (count($this->cartas)-1));
        $carta = $this->cartas[$indice];
        $this->sacarCarta($indice);
        return $carta ;
    }

    /**
     * [agrega una carta a la baraja]
     * @return []         
     */
    public function crear() {
        $this->cartas[] = $carta;
    }
}

?>
