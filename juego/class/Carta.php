<?php
/**
 * Colección de metodos del objeto carta 
 **/
class Carta {

    /******************************************************/
    /********************** ATRIBUTOS *********************/
    /******************************************************/
    private $nombre;
    private $valor;
    private $palo;

    /******************************************************/
    /*********************** METODOS **********************/
    /******************************************************/


    function __construct($nombre, $valor, $palo)
    {
        $this->nombre = $nombre;
        $this->valor = $valor;
        $this->palo = $palo;
    }
    /*********************** GETS **********************/
    public function getNombre () {
        return $this->nombre;
    }

    public function getValor () {
        return $this->valor;
    }

    public function getPalo () {
        return $this->palo;
    }
}
?>
