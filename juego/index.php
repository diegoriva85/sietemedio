<?php
//se influye el objeto baraja
require_once './class/Baraja.php';


/**
 * [verifica que el usuario introduzaca las teclas epseradas]
 * @return [string]         
 */
function checkEntri ($pedido)
{
	$tecla = readline($pedido);
	$tecla = (preg_match('/(c|r|a|p)/',$tecla))? $tecla :  checkEntri($pedido); 
	return $tecla;
} 

echo "\n\n*********************************************\n";
echo "   Bienvenido el 7 y media que lo disfrutes\n";
echo "*********************************************\n";

echo " -Gana quien se acerca mas a 7.5 tomando cartas \n  de la baraja.\n";
echo " -Las cartas van del 1 al 7 y respetan ese valor \n  y las figuras valen 0.5\n";
echo " -Si la maquina iguala tu puntaje gana.\n";
echo "******************** SUERTE ******************\n\n";

do {//inicio bucle general del juego termina si el usuario presiona "a"
//$salida = "a";
$salir_jugador = false; //varaible que indica el final del turno del jugador.
$salir_ai = true;		//varaible que indica el final del turno de la maquina.
$gana_jugador = true;   //varaible que indica si gana el juegador.
$cont_jugador = 0;		//varaible que guarda los puntos del usuario.
$cont_ai = 0;			//varaible que guarda los puntos da la maquina.
$valor_max = floatval(8); 
$baraja = new Baraja(); //se instancia una baraja.

	do {//inicio del turno del juegador
		$carta_act = $baraja->getCarta(); //se toma una carta
	    $cont_jugador += $carta_act->getValor();
	   	echo "Te toco la carta ".$carta_act->getNombre()." de ".$carta_act->getPalo()."\n";
	    echo "Tu puntaje actual es de ". $cont_jugador."\n"; //se muestran los puntos obtenidos al jugador
	    echo "\n";
	    //se consulta al jugador si quiere plantarse o continuar pidiendo cartas
		$line = ($cont_jugador >= $valor_max)? "" :checkEntri("Plantarse (p) o Continuar (c)?");
	
		switch (true) { // segun lo elegido por el jugador si detemina si sepaso,si continua o se planto.
			case (($cont_jugador >= $valor_max) && ($line === "c")) || ($line === ""): //se pasa y pierde
				$mensaje = "Te pasaste Gana la Maquina\n";
				$salir_jugador = true;
				$gana_jugador = false;
				break;
			case (($cont_jugador << $valor_max) && ($line === "p")): //se planta y continua la maquina
				echo "**********************************\n";
				echo " Te plantaste con ".$cont_jugador." puntos \n";
				echo "**********************************\n";
				echo "Comienza la maquina \n";
				$salir_jugador = true ;
				$salir_ai = false;
				break;
			case (($cont_jugador << $valor_max) && ($line === "c")): //pide otra carta
				$salir_jugador = false;
				break;
		}
	} while ($salir_jugador === false); //fin del turno del jugador 

	while($salir_ai === false && $gana_jugador == true) {  //inicio del turno de la maquina

	    $carta_act = $baraja->getCarta();
	    $cont_ai += $carta_act->getValor();
	   	echo "La maquina tomo la carta ".$carta_act->getNombre()." de ".$carta_act->getPalo();
	    echo " su puntaje actual es ". $cont_ai."\n";
	    echo "\n";
	    sleep(1);

		switch (true) {
			case ($cont_ai >= $cont_jugador) && ($cont_ai <= 7.5): //supera el puntaje del jugador y no se pasa
				$mensaje = "Gano la maquina\n";
				$gana_jugador = false;
				$salir_ai = true;
				break;
			case ($cont_ai >= 7.5): // se paso y gana el juegador
				$salir_ai = true;
				echo "La maquina se paso\n";
				break;
			default: //pide otra carta
				$salir_ai = false;
				break;
		}
	}
$mensaje = ($gana_jugador)? "Felicitaciones GANASTE con ". $cont_jugador." puntos\n" : $mensaje;  
echo "\n";
echo "**********************************\n";
echo $mensaje;
echo "**********************************\n";
$salida = checkEntri("Repetir (r) o Abandonar (a)?\n");
} while ($salida ==="r");

?>
